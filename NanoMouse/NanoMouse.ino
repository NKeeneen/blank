#include <Servo.h>

#define RIGHT 1
#define LEFT -1

Servo LeftServo;
Servo RightServo;

const byte LedPin = 13;
const byte buttonPin = D6;

const byte power = 100;

void forward()
{
  LeftServo.writeMicroseconds(1500 - power);
  RightServo.writeMicroseconds(1500 + power);
}

void stop(int time = 200)
{
  LeftServo.writeMicroseconds(1500);
  RightServo.writeMicroseconds(1500);
  delay(time);
}



void forwardTime(unsigned int degrees)
{
  forward(degrees);
  delay();
  stop();
}



void turn (int direction, int time)
{
  LeftServo.writeMicroseconds(1500 - power * direction);
  RightServo.writeMicroseconds(1500 - power * direction);
  delay(time);
  stop();
}

void setup()
{
  LeftServo.attach(10);
  RightServo.attach(D4);
  pinMode(LedPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);

  while (digitalRead(buttonPin))
  {
    yield();
  }

  turn(LEFT, 2000);
  turn(RIGHT,1900);
  turn(LEFT, 1100);
}
void loop()
{
  digitalWrite(LedPin, HIGH);

}